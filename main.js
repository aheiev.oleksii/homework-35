const inputForms = document.querySelectorAll('form'),
    http = document.getElementById('http'),
    https = document.getElementById('https'),
    httpButton = document.getElementById('http-form'),
    httpsButton = document.getElementById('https-form'),
    HTTP = 'http://',
    HTTPS = 'https://';

function linkValidation(link) {
    if (link.indexOf(HTTPS) !== 0 && link.indexOf(HTTP) !== 0) {
        document.location = HTTPS ? (link = HTTPS + link) : (link = HTTP + link);
    }
}

httpButton.addEventListener('click', () => {
    linkValidation(http.value);
});

httpsButton.addEventListener('click', () => {
    linkValidation(https.value);
});

inputForms.forEach((form) => {
    form.addEventListener('click', el => {
        el.preventDefault();
    });
});